<?php
/*
 * VivifierInterface.php
 */

namespace AzureSpring\Silo;

use Symfony\Component\HttpFoundation\File\File;

/**
 * VivifierInterface
 */
interface VivifierInterface
{
    /**
     * @param array         $path
     * @param SiloInterface $silo
     *
     * @return File|null
     */
    public function find(array $path, SiloInterface $silo): ?File;

    /**
     * @param string        $filename
     * @param array|null    $options
     * @param SiloInterface $silo
     *
     * @return string[]|null
     */
    public function wire(string $filename, ?array $options, SiloInterface $silo): ?array;
}
