<?php
/*
 * SiloInterface.php
 */
namespace AzureSpring\Silo;

use Symfony\Component\HttpFoundation\File\File;

/**
 * SiloInterface
 */
interface SiloInterface
{
    /**
     * @param string[] $path
     * @param bool     $wired
     *
     * @return File
     */
    public function find(array $path, bool $wired = false): File;

    /**
     * @param File $file
     *
     * @return string
     */
    public function save(File $file): string;

    /**
     * @param string     $filename
     * @param array|null $options
     *
     * @return string[]
     */
    public function wire(string $filename, ?array $options = null): array;
}
