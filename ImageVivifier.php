<?php
/*
 * ImageVivifier.php
 */

namespace AzureSpring\Silo;

use Symfony\Component\HttpFoundation\File\File;

/**
 * ImageVivifier
 */
class ImageVivifier implements VivifierInterface
{
    /**
     * @inheritDoc
     *
     * @throws \ImagickException
     */
    public function find(array $path, SiloInterface $silo): ?File
    {
        if (5 !== count($path)
            || !in_array($mode = $path[0], ['crop', 'fill'])
            || !preg_match('/(\d+),(\d+)/', $path[1], $m)) {
            return null;
        }

        [$_, $w, $h] = $m;
        $src = $silo->find(array_slice($path, 2));
        $img = new \Imagick($src->getPathname());
        switch ($mode) {
            case 'crop':
                $img->cropThumbnailImage($w, $h);
                break;

            case 'fill':
                $img->thumbnailImage($w, $h, true);
                break;
        }

        $dst = $silo->find($silo->wire(basename($src), [
            'mode'   => $mode,
            'width'  => $w,
            'height' => $h,
        ]), true);
        @mkdir(dirname($dst), 0777, true);
        $img->writeImageFile($file = fopen($dst->getPathname(), 'w'));
        fclose($file);

        return $dst;
    }

    /**
     * @inheritDoc
     */
    public function wire(string $filename, $options, SiloInterface $silo): ?array
    {
        if (!is_array($options) || !in_array(@$options['mode'], ['crop', 'fill'])) {
            return null;
        }

        return $silo->wire($filename, ['cd' => [$options['mode'], "{$options['width']},{$options['height']}"]]);
    }
}
