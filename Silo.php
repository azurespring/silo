<?php
/*
 * Silo.php
 */

namespace AzureSpring\Silo;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Silo
 */
class Silo implements SiloInterface
{
    private $root;

    private $vivifiers;

    /**
     * @param string              $root
     * @param VivifierInterface[] $vivifiers
     */
    public function __construct(string $root, array $vivifiers = [])
    {
        $this->root = $root;
        $this->vivifiers = $vivifiers;
    }

    /**
     * @inheritDoc
     */
    public function find(array $path, bool $wired = false): File
    {
        if ($wired) {
            array_unshift($path, $this->root);

            return new File(implode(DIRECTORY_SEPARATOR, $path), false);
        }

        foreach ($this->vivifiers as $v) {
            if ($f = $v->find($path, $this)) {
                return $f;
            }
        }

        if (3 === count($path)
            && preg_match('/^[[:xdigit:]]{40}$/', pathinfo($filename = $path[2], PATHINFO_FILENAME))
            && substr($filename, 0, 2) === $path[0]
            && substr($filename, 2, 2) === $path[1]) {
            return $this->find($path, true);
        }

        throw new FileNotFoundException(implode(DIRECTORY_SEPARATOR, $path));
    }

    /**
     * @inheritDoc
     */
    public function save(File $file): string
    {
        $filename = sha1_file($file->getPathname());
        if ($ext = $file->guessExtension()) {
            $filename .= ".{$ext}";
        }

        $directory = dirname($this->find($this->wire($filename), true));
        $file->move($directory, $filename);

        return $filename;
    }

    /**
     * @inheritDoc
     */
    public function wire(string $filename, ?array $options = null): array
    {
        foreach ($this->vivifiers as $v) {
            if ($path = $v->wire($filename, $options, $this)) {
                return $path;
            }
        }

        $path = [
            substr($filename, 0, 2),
            substr($filename, 2, 2),
            $filename,
        ];
        if (is_array($options) && array_key_exists('cd', $options)) {
            array_unshift($path, ...$options['cd']);
        }

        return $path;
    }
}
