<?php

namespace AzureSpring\Silo;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\File;

class ImageVivifierTest extends TestCase
{
    /**
     * @test
     *
     * @throws \ImagickException
     * @throws \ReflectionException
     */
    public function findOK()
    {
        vfsStream::setup('t');
        @mkdir(vfsStream::url('t/silo/2a/51'), 0777, true);
        copy(__DIR__.'/DSC05448.jpg', vfsStream::url('t/silo/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'));

        $silo = $this->createMock(SiloInterface::class);
        $silo
            ->method('find')
            ->will($this->returnValueMap([
                [['2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], false, new File(vfsStream::url('t/silo/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), false)],
                [['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], true, new File(vfsStream::url('t/silo/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), false)],
            ]))
        ;
        $silo
            ->method('wire')
            ->with($this->equalTo('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), $this->equalTo(['mode' => 'crop', 'width' => 400, 'height' => 400]))
            ->willReturn(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'])
        ;

        $vivifier = new ImageVivifier();

        $this->assertEquals(
            new File(vfsStream::url('t/silo/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), false),
            $vivifier->find(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], $silo)
        );
        $this->assertFileExists(vfsStream::url('t/silo/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'));
        $this->assertGreaterThan(5, filesize(vfsStream::url('t/silo/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg')));
    }

    /**
     * @test
     * @throws \ReflectionException
     */
    public function wireOK()
    {
        $silo = $this->createMock(SiloInterface::class);
        $silo
            ->method('wire')
            ->with($this->equalTo('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), $this->equalTo(['cd' => ['crop', '400,400']]))
            ->willReturn(['crop', '400,400', '25', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'])
        ;

        $vivifier = new ImageVivifier();

        $this->assertEquals(
            ['crop', '400,400', '25', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'],
            $vivifier->wire('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg', ['mode' => 'crop', 'width' => 400, 'height' => 400], $silo)
        );
    }
}
