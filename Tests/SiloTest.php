<?php

namespace AzureSpring\Silo;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

final class SiloTest extends TestCase
{
    /**
     * @test
     */
    public function constructOK()
    {
        $silo = new Silo('/path/to/silo');

        $this->assertInstanceOf(Silo::class, $silo);
    }

    /**
     * @test
     * @dataProvider findWiredProvider
     */
    public function findWiredOK($expected, $path)
    {
        $silo = new Silo('/path/to/silo');

        $this->assertEquals($expected, $silo->find($path, true));
    }

    public function findWiredProvider()
    {
        return [
            ['/path/to/silo/hello/world', ['hello', 'world']],
            ['/path/to/silo/63/2f/632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg', ['63', '2f', '632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg']],
            ['/path/to/silo/.o/63/2f/632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg', ['.o', '63', '2f', '632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg']],
        ];
    }

    /**
     * @test
     */
    public function findLegalNonWiredOK()
    {
        $silo = new Silo('/path/to/silo');

        $this->assertEquals('/path/to/silo/63/2f/632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg', $silo->find(['63', '2f', '632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg']));
    }

    /**
     * @test
     * @dataProvider findIllegalNonWiredProvider
     */
    public function findIllegalNonWiredError($path)
    {
        $silo = new Silo('/path/to/silo');

        $this->expectException(FileNotFoundException::class);
        $silo->find($path);
    }

    public function findIllegalNonWiredProvider()
    {
        return [
            [['hello', 'world']],
            [['path', 'to', 'file']],
            [['crop', '400,400', '63', '2f', '632f4e4a6c4951a250e6a4c7c83c8f21b35f9522.jpeg']],
        ];
    }

    /**
     * @test
     * @throws \ReflectionException
     */
    public function findWithVivifierOK()
    {
        $file = new File('/path/to/silo/crop/400,400/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg', false);
        $vivifier = $this->createMock(VivifierInterface::class);
        $vivifier
            ->method('find')
            ->with($this->equalTo(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg']))
            ->willReturn($file)
        ;

        $silo = new Silo('/path/to/silo', [$vivifier]);

        $this->assertEquals($file, $silo->find(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg']));
    }

    /**
     * @test
     */
    public function saveOK()
    {
        vfsStream::setup('t');
        copy(__DIR__.'/DSC05448.jpg', vfsStream::url('t/DSC05448.jpg'));

        $silo = new Silo(vfsStream::url('t/silo'));

        $this->assertEquals('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg', $silo->save(new File(vfsStream::url('t/DSC05448.jpg'))));
        $this->assertFileExists(vfsStream::url('t/silo/2a/51/2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'));
    }

    /**
     * @test
     * @dataProvider wireProvider
     */
    public function wireOK($path, $filename, $options = null)
    {
        $silo = new Silo('/path/to/silo');

        $this->assertEquals($path, $silo->wire($filename, $options));
    }

    public function wireProvider()
    {
        return [
            [['2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'],
            [['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg', ['cd' => ['crop', '400,400']]],
        ];
    }

    /**
     * @test
     * @throws \ReflectionException
     */
    public function wireWithVivifierOK()
    {
        $vivifier = $this->createMock(VivifierInterface::class);
        $vivifier
            ->method('wire')
            ->with($this->equalTo('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'), $this->equalTo(['mode' => 'crop', 'width' => 400, 'height' => 400]))
            ->willReturn(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'])
        ;

        $silo = new Silo('/path/to/silo', [$vivifier]);

        $this->assertEquals(['crop', '400,400', '2a', '51', '2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg'], $silo->wire('2a51db7686b9e5d122c72ec8c5eef93aeafb4988.jpeg', ['mode' => 'crop', 'width' => 400, 'height' => 400]));
    }
}
